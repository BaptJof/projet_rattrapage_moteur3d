
#ifndef _EFFECT_BLOOM_
#define _EFFECT_BLOOM_

#include "Core/Engine/OpenGL/EffectGL.h"
#include <memory.h>
#include "Engine/OpenGL/ModelGL.h"
#include "Effects/Blur/Blur.h"
class Bloom : public EffectGL
{
	public:
		Bloom(std::string name, int finalsize = 512, float bloomTreshold = 0.5);
		~Bloom();

		virtual void apply(GPUFBO* in, GPUFBO* out);

		void setBloomTreshold(float v);

	protected:
		GPUFBO *temp;
		GPUFBO* bBuffers[4];
		int fboSize;
		int numBuffers;

		Blur* blur[4];

		GPUfloat  *bloomTreshold;
		GPUsampler *bloomSampler, *bloomSampler1, *bloomSampler2, *bloomSampler3, *fboIn;
		GLProgram *bloom, *copy, *pass;
		GLProgram* vp;
		float m_bloomTreshold;

}; 
#endif // !_EFFECT_BLOOM_
