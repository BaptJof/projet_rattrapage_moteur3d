#version 430

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Lighting/Lighting"
#line 6 

uniform sampler2D texFramebuffer;

out vec4 Color;

const float blurSizeH = 1.0 / 1024;
const float blurSizeV = 1.0 / 1024;

in vec3 texCoord;

void main() {
    vec4 sum = vec4(0.0);
    for (int x = -4; x <= 4; x++) {
        for (int y = -4; y <= 4; y++) {
            sum += texture(texFramebuffer, vec2(texCoord.x + x * blurSizeH, texCoord.y + y * blurSizeV)) / 81.0;
        }
    }
    Color = sum;
}