
#include "Blur.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Engine.h"



Blur::Blur(std::string name):
	EffectGL(name,"Blur")
{
	int bsize = 1024;
	vp = new GLProgram(this->m_ClassName + "-Base", GL_VERTEX_SHADER);
	fpV = new GLProgram(this->m_ClassName + "-BlurShader", GL_FRAGMENT_SHADER);

	m_ProgramPipeline->useProgramStage(GL_VERTEX_SHADER_BIT, vp);
	m_ProgramPipeline->useProgramStage(GL_FRAGMENT_SHADER_BIT, fpV);
	m_ProgramPipeline->link();

	text = fpV->uniforms()->getGPUsampler("texFramebuffer");
	text->Set(0);


	temp = new GPUFBO("FBO-Blur");
	temp->createTexture2DAttachments(bsize, bsize, 1, false);
	
}
Blur::~Blur()
{
	delete temp;
	delete vp;
	delete fpV;
	
}


void Blur::apply(GPUFBO *in)
{

	m_ProgramPipeline->useProgramStage(GL_FRAGMENT_SHADER_BIT, fpV);
	temp->enable();
	in->bindColorTexture(0);
	m_ProgramPipeline->bind();
	drawQuad();
	m_ProgramPipeline->release();
	in->releaseColorTexture();
	temp->disable();

	in->enable();
	temp->bindColorTexture(0);
	m_ProgramPipeline->bind();
	quad->drawGeometry(GL_TRIANGLES);
	m_ProgramPipeline->release();
	temp->releaseColorTexture();
	in->disable();

}
void Blur::displayInterface()
{

}

