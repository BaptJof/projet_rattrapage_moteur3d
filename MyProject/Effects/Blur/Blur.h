
#ifndef _GOBLIM_BLUR_EFFECT
#define _GOBLIM_BLUR_EFFECT

#include "Engine/OpenGL/EffectGL.h"
class Blur : public EffectGL
{
	public:
		Blur(std::string name);
		~Blur();

		virtual void apply(GPUFBO *in);
		virtual void displayInterface();

		


	
	protected:
		GPUFBO* temp;
		GPUsampler* text;
		GLProgram *vp;
		GLProgram* fpV;
		

};
#endif
