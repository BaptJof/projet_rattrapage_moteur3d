/*
 *	(c) XLim, UMR-CNRS
 *	Authors: G.Gilet
 *
 */

#ifndef _SAMPLE_ENGINE_H
#define _SAMPLE_ENGINE_H

#include <map>
#include <string>
#include "Engine/OpenGL/EngineGL.h"
#include "Blur/Blur.h"
#include "Bloom/Bloom.h"
#include "Core/Effects/PostProcess/AntiAliasing/AntiAliasing.h"


class SampleEngine : public EngineGL
{
	public:
		SampleEngine(int width, int height);
		~SampleEngine();

		virtual bool init(std::string filename = "");
		virtual void render();
		virtual void animate(const int elapsedTime);
		virtual double norme(glm::vec3 u, glm::vec3 v); //Norme de la position de l'�clair 'u' et de l'axe x 'v'
		GPUFBO* fbo2;
		Bloom* bloom;
		double rayon = 0.5;
		double* angles; //Valeurs en radian des angles des �clairs en fonction de leur position avec le cr�ne centrale

		

	protected:
	

};
#endif