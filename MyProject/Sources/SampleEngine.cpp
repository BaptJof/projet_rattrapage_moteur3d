﻿/*
EngineGL overloaded for custom rendering
*/
#include "SampleEngine.h"
#include "Engine/OpenGL/v4/GLProgram.h"
#include "Engine/OpenGL/SceneLoaderGL.h"
#include "Engine/Base/NodeCollectors/MeshNodeCollector.h"
#include "Engine/Base/NodeCollectors/FCCollector.h"

#include "Lightning/Lightning.h"
#include "Skull/Skull.h"
#include "SkullFlame/SkullFlame.h"
#include "TPMaterial/TPMaterial.h"

#include "GPUResources/GPUInfo.h"


SampleEngine::SampleEngine(int width, int height) :
EngineGL (width, height)
{
	LOG_INFO << "# - " << __FUNCTION__ << std::endl;

	fbo2 = scene->getResource<GPUFBO>("monFBO2");
	fbo2->createTexture2DAttachments(1024, 1024);
	
	bloom = scene->getEffect<Bloom>("Bloom");

	angles = new double[4];

}

SampleEngine::~SampleEngine()
{


}


bool SampleEngine::init(std::string filename)
{
	//Couleur des éclairs
	glm::vec3 colLight = glm::vec3(153.0 / 255.0, 1.0, 1.0); //Bleu clair
	glm::vec3 colLight2 = glm::vec3(102.0 / 255.0, 1.0, 102.0 / 255.0); //Vert
	glm::vec3 colLight3 = glm::vec3(1.0, 153.0/255.0, 51.0 / 255.0); //Orange
	glm::vec3 colLight4 = glm::vec3(1.0, 1.0, 102.0 / 255.0); //Jaune
	Lightning* lightningMat = new Lightning("Lightning", colLight);
	Lightning* lightningMat2 = new Lightning("Lightning2", colLight2);
	Lightning* lightningMat3 = new Lightning("Lightning3", colLight3);
	Lightning* lightningMat4 = new Lightning("Lightning4", colLight4);

	glm::vec3* position = new glm::vec3[4];
	glm::vec3* color = new glm::vec3[4];

	color[0] = colLight4;
	color[1] = colLight3;
	color[2] = colLight2;
	color[3] = colLight;


	//Ajout des 4 éclairs
	Node* eclair = scene->getNode("eclair");
	eclair->setModel(scene->m_Models.get<ModelGL>(ressourceCoreObjPath + "Eclair.obj"));
	glm::vec3 vec1 = glm::vec3(-rayon, 0, 0);
	position[0] = vec1;
	eclair->frame()->translate(vec1);
	angles[0] = norme(vec1, glm::vec3(1, 0, 0));
	eclair->frame()->scale(glm::vec3(0.2));
	eclair->setMaterial(lightningMat);
	scene->getSceneNode()->adopt(eclair);

	Node* eclair2 = scene->getNode("eclair2");
	eclair2->setModel(scene->m_Models.get<ModelGL>(ressourceCoreObjPath + "Eclair.obj"));
	glm::vec3 vec2 = glm::vec3(0, 0, -rayon);
	position[1] = vec2;
	eclair2->frame()->translate(vec2);
	angles[1] = -norme(vec2, glm::vec3(1, 0, 0));
	eclair2->frame()->scale(glm::vec3(0.2));
	eclair2->setMaterial(lightningMat2);
	scene->getSceneNode()->adopt(eclair2);

	Node* eclair3 = scene->getNode("eclair3");
	scene->getSceneNode()->adopt(eclair3);
	eclair3->setModel(scene->m_Models.get<ModelGL>(ressourceCoreObjPath + "Eclair.obj"));
	glm::vec3 vec3 = glm::vec3(0, 0, rayon);
	position[2] = vec3;
	eclair3->frame()->translate(vec3);
	angles[2] = norme(vec3, glm::vec3(1, 0, 0));
	eclair3->frame()->scale(glm::vec3(0.2));
	eclair3->setMaterial(lightningMat3);
	

	Node* eclair4 = scene->getNode("eclair4");
	scene->getSceneNode()->adopt(eclair4);
	eclair4->setModel(scene->m_Models.get<ModelGL>(ressourceCoreObjPath + "Eclair.obj"));
	glm::vec3 vec4 = glm::vec3(rayon, 0, 0);
	position[3] = vec4;
	eclair4->frame()->translate(vec4);
	angles[3] = norme(vec4, glm::vec3(1, 0, 0));
	eclair4->frame()->scale(glm::vec3(0.2));
	eclair4->setMaterial(lightningMat4);
	

	//Ajout du crâne entrale
	Skull* skullMat = new Skull("Skull", color, position, angles);

	Node* skull = scene->getNode("skull");
	scene->getSceneNode()->adopt(skull);
	skull->setModel(scene->m_Models.get<ModelGL>(ressourceCoreObjPath + "Skull.obj"));
	skull->frame()->rotate(glm::vec3(0, 1, 0), 10);
	skull->frame()->scale(glm::vec3(0.2));
	skull->setMaterial(skullMat);

	//Ajout des crânes adjacents horizontaux au crâne centrale

	SkullFlame* skullFlameMat = new SkullFlame("SkullFlame");

	Node* skullFlame = scene->getNode("skullFlame");
	scene->getSceneNode()->adopt(skullFlame);
	skullFlame->setModel(scene->m_Models.get<ModelGL>(ressourceCoreObjPath + "Skull.obj"));
	skullFlame->frame()->translate(glm::vec3(10, 0, -6));
	skullFlame->frame()->rotate(glm::vec3(0, 1, 0), 10);
	skullFlame->frame()->scale(glm::vec3(0.2));
	skullFlame->setMaterial(skullFlameMat);

	Node* skullFlame2 = scene->getNode("skullFlame2");
	scene->getSceneNode()->adopt(skullFlame2);
	skullFlame2->setModel(scene->m_Models.get<ModelGL>(ressourceCoreObjPath + "Skull.obj"));
	skullFlame2->frame()->translate(glm::vec3(-10, 0, 6));
	skullFlame2->frame()->rotate(glm::vec3(0, 1, 0), 10);
	skullFlame2->frame()->scale(glm::vec3(0.2));
	skullFlame2->setMaterial(skullFlameMat);
	

	setUpEngine();
	LOG_INFO << "initialisation complete" << std::endl;
	return(true);
}


void SampleEngine::render ()
{
	fbo2->enable();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for (unsigned int i = 0; i < renderedNodes->nodes.size()-3; i++)
		renderedNodes->nodes[i]->render();
	drawBBAndLight();
	fbo2->disable();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//Application du bloom sur les éclairs
	bloom->apply(fbo2, fbo2);
	fbo2->display();

	renderedNodes->nodes[renderedNodes->nodes.size()-3]->render();
	renderedNodes->nodes[renderedNodes->nodes.size() - 2]->render();
	renderedNodes->nodes[renderedNodes->nodes.size() - 1]->render();

}

void SampleEngine::animate (const int elapsedTime)
{
	//Mouvement des éclairs autour du crâne centrale
	for (int i = 0; i < renderedNodes->nodes.size() - 3; i++) {
		float phi = angles[i] + (elapsedTime/1000);
		renderedNodes->nodes[i]->frame()->translate(glm::vec3(rayon * glm::cos(phi), 0, rayon*glm::sin(phi)));
	}

	//Rotation des crânes rouge et bleu sur eux-mêmes
	renderedNodes->nodes[renderedNodes->nodes.size() - 2]->frame()->rotate(glm::vec3(0, 0, 1), 0.01);
	renderedNodes->nodes[renderedNodes->nodes.size() - 1]->frame()->rotate(glm::vec3(0, 0, 1), 0.01);
	for (unsigned int i = 0; i < allNodes->nodes.size() ; i++) {
		allNodes->nodes[i]->animate(elapsedTime);
	}
	// force update of lighting model
	lightingModel->update (true);
	Scene::getInstance()->camera()->updateBuffer();
}

double SampleEngine::norme(glm::vec3 u, glm::vec3 v)
{
	double scalaire = glm::dot(u, v);
	double normeU = glm::sqrt(u.x * u.x + u.y*u.y + u.z * u.z);
	double normeV = glm::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	double acos = glm::acos(scalaire / (normeU * normeV));
	LOG_INFO << acos << std::endl;
	return acos;
}

