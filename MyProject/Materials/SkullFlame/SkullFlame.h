#ifndef _SKULLFLAME_H
#define _SKULLFLAME_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class SkullFlame : public MaterialGL
{
public:
	SkullFlame(std::string name);
	~SkullFlame();
	void setColor(glm::vec4& c);

	virtual void render(Node* o);
	virtual void update(Node* o, const int elapsedTime);
	GPUmat4* modelViewProj;
	GPUvec3* posLumGPU;
	GPUvec3* camLumGPU;
	GPUfloat* temps;

};

#endif