#version 430

#extension GL_ARB_shading_language_include : enable
#line 6 



in vec3 v_Color;

in vec3 vViewLum;
in vec3 vNormal;

in vec3 vLum;
in vec3 newPosition;
in float tempsF;

layout(location = 0) out vec4 Color;

void main()
{
	vec3 colorLum;

	vec3 color;

	vec3 norm = normalize(vNormal);
	vec3 lumFrag = normalize(vLum);

	//Ajout de la couleur pour chaque fragment
	double red = 24 - newPosition.z;
	red = (red / 24) + (tempsF/10000.0); //Changement de red au cours du temps
	while (red > 1.0) {
		red = red - 1.0;
	}
	double blue = 1.0 - red;
	

	color = vec3(red, 0, blue);
	
	//Lumi�re de Phong
	vec3 colorDiff = color * max(dot(norm, lumFrag), 0.0);

	vec3 view = normalize(vViewLum);
	vec3 refl = reflect(-lumFrag, norm);

	vec3 colorSpe = vec3(1.0, 1.0, 1.0) * pow(max(dot(view, refl), 0.0), 100);

	colorLum = (color * 0.1) + (colorDiff * 0.7) + (colorSpe * 0.2);

	Color = vec4(colorLum, 1.0);

}