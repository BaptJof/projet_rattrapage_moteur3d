#include "SkullFlame.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


SkullFlame::SkullFlame(std::string name) :
	MaterialGL(name, "SkullFlame")
{
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");

	GPUvec3* colorCPU = vp->uniforms()->getGPUvec3("CPUColor");
	colorCPU->Set(glm::vec3(224.0/255.0, 224.0/255.0, 224.0/255.0));

	temps = vp->uniforms()->getGPUfloat("temps");
	temps->Set(0);

	posLumGPU = vp->uniforms()->getGPUvec3("posLum");
	posLumGPU->Set(glm::vec3(0, 30, 0));

	camLumGPU = vp->uniforms()->getGPUvec3("camLum");
	camLumGPU->Set(glm::vec3(0, 0, 0));


}
SkullFlame::~SkullFlame()
{

}

void SkullFlame::setColor(glm::vec4& c)
{

}

void SkullFlame::render(Node* o)
{

	modelViewProj->Set(o->frame()->getTransformMatrix());

	if (m_ProgramPipeline)
	{
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void SkullFlame::update(Node* o, const int elapsedTime)
{
	temps->Set(elapsedTime);
	Frame* frame = Scene::getInstance()->frame();
	Camera* camera = Scene::getInstance()->camera();
	posLumGPU->Set(frame->convertPtTo(glm::vec3(0, 30, 0), o->frame()));
	camLumGPU->Set(camera->convertPtTo(glm::vec3(0, 0, 0), o->frame()));
}
