#include "TPMaterial.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


TPMaterial::TPMaterial(std::string name) :
	MaterialGL(name, "TPMaterial")
{
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");

	GPUvec3* colorCPU = vp->uniforms()->getGPUvec3("CPUColor");
	colorCPU->Set(glm::vec3(0.0, 0.0, 1.0));

	time_t now = time(0);
	tm* ltm = localtime(&now);
	float seconds = ltm->tm_sec;
	GPUfloat* temps = vp->uniforms()->getGPUfloat("temps");
	temps->Set(seconds);

	posLumGPU = vp->uniforms()->getGPUvec3("posLum");
	posLumGPU->Set(glm::vec3(0, 30, 0));

	camLumGPU = vp->uniforms()->getGPUvec3("camLum");
	camLumGPU->Set(glm::vec3(0, 0, 0));

}
TPMaterial::~TPMaterial()
{

}

void TPMaterial::setColor(glm::vec4& c)
{

}

void TPMaterial::render(Node* o)
{

	modelViewProj->Set(o->frame()->getTransformMatrix());

	if (m_ProgramPipeline)
	{
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void TPMaterial::update(Node* o, const int elapsedTime)
{
	Frame* frame = Scene::getInstance()->frame();
	Camera* camera = Scene::getInstance()->camera();
	posLumGPU->Set(frame->convertPtTo(glm::vec3(0, 30, 0), o->frame()));
	camLumGPU->Set(camera->convertPtTo(glm::vec3(0, 0, 0), o->frame()));
}
