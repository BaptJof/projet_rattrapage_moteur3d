#version 430

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Lighting/Lighting"
#line 6 



in vec3 v_Color;

in vec3 vViewLum;
in vec3 vNormal;
in vec3 vLum;

layout(location = 0) out vec4 Color;

void main()
{
	vec3 colorLum;

	vec3 color;

	vec3 norm = normalize(vNormal);
	vec3 lumFrag = normalize(vLum);
	color = vec3(0, 0, 1);

	vec3 colorDiff = color * max(dot(norm, lumFrag), 0.0);

	vec3 view = normalize(vViewLum);
	vec3 refl = reflect(-lumFrag, norm);

	vec3 colorSpe = vec3(1.0, 1.0, 1.0) * pow(max(dot(view, refl), 0.0), 100);

	colorLum = (color * 0.1) + (colorDiff * 0.7) + (colorSpe * 0.2);

	Color = vec4(v_Color, 1.0);

}