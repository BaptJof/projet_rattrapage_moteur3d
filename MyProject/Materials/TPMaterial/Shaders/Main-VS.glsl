#version 430

#extension GL_ARB_shading_language_include : enable
#include "/Materials/Common/Common"
#line 6 


layout(std140) uniform CPU
{
    mat4 MVP;
    vec3 CPUColor;
    float temps;
    vec3 posLum;
    vec3 camLum;
};

out gl_PerVertex{
       vec4 gl_Position;
       float gl_PointSize;
       float gl_ClipDistance[];
};
layout(location = 0) in vec3 Position;
layout(location = 2) in vec3 Normal;
out vec3 v_Color;

out vec3 vViewLum;
out vec3 vNormal;
out vec3 vLum;

void main()
{

    vec3 NewPosition = Position;

    vec3 vecLum = posLum - NewPosition;

    gl_Position = MVP * vec4(NewPosition, 1.0);

    vNormal = normalize(Normal);
    vLum = normalize(vecLum);


    vec3 viewLum = camLum - NewPosition;

    vViewLum = normalize(viewLum);


    v_Color = CPUColor;


}
