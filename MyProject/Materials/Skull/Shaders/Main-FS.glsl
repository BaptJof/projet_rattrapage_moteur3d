#version 430

#extension GL_ARB_shading_language_include : enable
#line 6 



in vec3 v_Color;

in vec3 vViewLum;
in vec3 vNormal;

in vec3 vLum;
in vec3 vLum2;
in vec3 vLum3;
in vec3 vLum4;

in vec3 color;
in vec3 color2;
in vec3 color3;
in vec3 color4;

layout(location = 0) out vec4 Color;

//Fonction de lumi�re de Phong
vec3 calcColorLum(vec3 lum, vec3 color, vec3 diff, vec3 spe) {

	vec3 norm = normalize(vNormal);
	vec3 lumFrag = normalize(lum);

	vec3 colorDiff = diff * max(dot(norm, lumFrag), 0.0);

	vec3 view = normalize(vViewLum);
	vec3 refl = reflect(-lumFrag, norm);

	vec3 colorSpe = spe * pow(max(dot(view, refl), 0.0), 100);

	vec3 colorLum = (color * 0.1) + (colorDiff * 0.6) + (colorSpe * 0.3);
	return colorLum;
}



void main()
{
	vec3 colorLum;

	vec3 spe = vec3(1.0, 1.0, 1.0);

	colorLum += calcColorLum(vLum, v_Color, color, spe);
	colorLum += calcColorLum(vLum2, v_Color, color2, spe);
	colorLum += calcColorLum(vLum3, v_Color, color3, spe);
	colorLum += calcColorLum(vLum4, v_Color, color4, spe);


	Color = vec4(colorLum, 1.0);

}