#version 430

#extension GL_ARB_shading_language_include : enable
#line 6 


layout(std140) uniform CPU
{
    mat4 MVP;
    vec3 CPUColor;
    float temps;
    vec3 camLum;
    vec3 light1;
    vec3 light2;
    vec3 light3;
    vec3 light4;

    vec3 pos1;
    vec3 pos2;
    vec3 pos3;
    vec3 pos4;
};

out gl_PerVertex{
       vec4 gl_Position;
       float gl_PointSize;
       float gl_ClipDistance[];
};
layout(location = 0) in vec3 Position;
layout(location = 2) in vec3 Normal;
out vec3 v_Color;

out vec3 vViewLum;
out vec3 vNormal;

out vec3 vLum;
out vec3 vLum2;
out vec3 vLum3;
out vec3 vLum4;

out vec3 color;
out vec3 color2;
out vec3 color3;
out vec3 color4;

void main()
{

    vec3 NewPosition = Position;

    gl_Position = MVP * vec4(NewPosition, 1.0);

    vNormal = normalize(Normal);

    //Normalisation des vecteurs lumi�res

    vec3 vecLum = pos1 - NewPosition;
    vLum = normalize(vecLum);

    vecLum = pos2 - NewPosition;
    vLum2 = normalize(vecLum);

    vecLum = pos3 - NewPosition;
    vLum3 = normalize(vecLum);

    vecLum = pos4 - NewPosition;
    vLum4 = normalize(vecLum);

    vec3 viewLum = camLum - NewPosition;

    vViewLum = normalize(viewLum);

    color = light1;
    color2 = light2;
    color3 = light3;
    color4 = light4;


    v_Color = CPUColor;


}
