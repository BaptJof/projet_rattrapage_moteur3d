#include "Skull.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


Skull::Skull(std::string name, glm::vec3* cols, glm::vec3* pos, double* angle) :
	MaterialGL(name, "Skull")
{
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");

	GPUvec3* colorCPU = vp->uniforms()->getGPUvec3("CPUColor");
	colorCPU->Set(glm::vec3(224.0/255.0, 224.0/255.0, 224.0/255.0));

	camLumGPU = vp->uniforms()->getGPUvec3("camLum");
	camLumGPU->Set(glm::vec3(0, 0, 0));

	GPUvec3* light1 = vp->uniforms()->getGPUvec3("light1");
	GPUvec3* light2 = vp->uniforms()->getGPUvec3("light2");
	GPUvec3* light3 = vp->uniforms()->getGPUvec3("light3");
	GPUvec3* light4 = vp->uniforms()->getGPUvec3("light4");
	light1->Set(cols[0]);
	light2->Set(cols[1]);
	light3->Set(cols[2]);
	light4->Set(cols[3]);

	position = pos;

	GPUvec3* pos1 = vp->uniforms()->getGPUvec3("pos1");
	GPUvec3* pos2 = vp->uniforms()->getGPUvec3("pos2");
	GPUvec3* pos3 = vp->uniforms()->getGPUvec3("pos3");
	GPUvec3* pos4 = vp->uniforms()->getGPUvec3("pos4");
	pos1->Set(position[0]);
	pos2->Set(position[1]);
	pos3->Set(position[2]);
	pos4->Set(position[3]);

	angles = angle;

}
Skull::~Skull()
{

}

void Skull::setColor(glm::vec4& c)
{

}

void Skull::render(Node* o)
{

	modelViewProj->Set(o->frame()->getTransformMatrix());

	if (m_ProgramPipeline)
	{
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void Skull::update(Node* o, const int elapsedTime)
{
	//Mise � jour des positions des �clairs en fonction du temps
	for (int i = 0; i < 4; i++) {
		float phi = angles[i] + (elapsedTime / 1000);
		position[i] = position[i] + glm::vec3(0.5 * glm::cos(phi), 0, 0.5 * glm::sin(phi));
	}
	GPUvec3* pos1 = vp->uniforms()->getGPUvec3("pos1");
	GPUvec3* pos2 = vp->uniforms()->getGPUvec3("pos2");
	GPUvec3* pos3 = vp->uniforms()->getGPUvec3("pos3");
	GPUvec3* pos4 = vp->uniforms()->getGPUvec3("pos4");
	pos1->Set(position[0]);
	pos2->Set(position[1]);
	pos3->Set(position[2]);
	pos4->Set(position[3]);
	Frame* frame = Scene::getInstance()->frame();
	Camera* camera = Scene::getInstance()->camera();
	camLumGPU->Set(camera->convertPtTo(glm::vec3(0, 0, 0), o->frame()));
}
