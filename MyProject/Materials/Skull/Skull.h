#ifndef _SKULL_H
#define _SKULL_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class Skull : public MaterialGL
{
public:
	Skull(std::string name, glm::vec3* cols, glm::vec3* pos, double* angle);
	~Skull();
	void setColor(glm::vec4& c);

	virtual void render(Node* o);
	virtual void update(Node* o, const int elapsedTime);
	GPUmat4* modelViewProj;
	GPUvec3* camLumGPU;
	glm::vec3* position; // Position des �clairs � l'instant t
	double* angles; // Angles initiaux des �clairs

};

#endif