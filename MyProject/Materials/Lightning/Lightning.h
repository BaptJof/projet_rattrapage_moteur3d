#ifndef _LIGHTNING_H
#define _LIGHTNING_H


#include "Engine/OpenGL/MaterialGL.h"
#include "Engine/OpenGL/Lighting/LightingModelGL.h"
#include <memory.h>

class Lightning : public MaterialGL
{
public:
	Lightning(std::string name, glm::vec3 color);
	~Lightning();
	void setColor(glm::vec4& c);

	virtual void render(Node* o);
	virtual void update(Node* o, const int elapsedTime);
	GPUmat4* modelViewProj;

};

#endif