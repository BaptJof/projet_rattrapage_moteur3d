#include "Lightning.h"
#include "Engine/Base/Node.h"
#include "Engine/Base/Scene.h"


Lightning::Lightning(std::string name, glm::vec3 color) :
	MaterialGL(name, "Lightning")
{
	modelViewProj = vp->uniforms()->getGPUmat4("MVP");

	GPUvec3* colorCPU = vp->uniforms()->getGPUvec3("CPUColor");
	colorCPU->Set(color);

}
Lightning::~Lightning()
{

}

void Lightning::setColor(glm::vec4& c)
{

}

void Lightning::render(Node* o)
{

	modelViewProj->Set(o->frame()->getTransformMatrix());

	if (m_ProgramPipeline)
	{
		m_ProgramPipeline->bind();
		o->drawGeometry(GL_TRIANGLES);
		m_ProgramPipeline->release();
	}
}

void Lightning::update(Node* o, const int elapsedTime)
{
	Frame* frame = Scene::getInstance()->frame();
	Camera* camera = Scene::getInstance()->camera();
}
